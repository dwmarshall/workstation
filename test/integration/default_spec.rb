describe command 'cinc-client --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command 'cinc-solo --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command 'chef-client --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Redirecting to cinc-client/ }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command 'chef-solo --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Redirecting to cinc-solo/ }
  its('stdout') { should match /^Cinc Client:/ }
end

describe command '/opt/cinc-workstation/embedded/bin/cinc-zero --version' do
  its('exit_status') { should eq 0 }
end

describe command 'cinc-auditor version' do
  its('exit_status') { should eq 0 }
end

describe command 'cinc-auditor detect' do
  its('exit_status') { should eq 0 }
end

describe command 'inspec version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /^Redirecting to cinc-auditor/ }
end

semver = /[0-9]+\.[0-9]+\.[0-9]+/

%w(cinc cinc-cli).each do |cinc|
  describe command "#{cinc} --version" do
    its('exit_status') { should eq 0 }
    its('stdout') { should_not match /[cC]hef/ }
    its('stdout') { should match /^Cinc Workstation version: #{semver}$/ }
    its('stdout') { should match /^Cinc Client version: #{semver}$/ }
    its('stdout') { should match /^Cinc Auditor version: #{semver}$/ }
    its('stdout') { should match /^Cinc CLI version: #{semver}$/ }
    its('stdout') { should match /^Test Kitchen version: #{semver}$/ }
    its('stdout') { should match /^Cookstyle version: #{semver}$/ }
  end
end

%w(chef chef-cli).each do |chef|
  describe command "#{chef} --version" do
    its('exit_status') { should eq 0 }
    its('stdout') { should match /^Redirecting to #{chef.gsub('chef', 'cinc')}...$/ }
    its('stdout') { should match /^Cinc Workstation version: #{semver}$/ }
    its('stdout') { should match /^Cinc Client version: #{semver}$/ }
    its('stdout') { should match /^Cinc Auditor version: #{semver}$/ }
    its('stdout') { should match /^Cinc CLI version: #{semver}$/ }
    its('stdout') { should match /^Test Kitchen version: #{semver}$/ }
    its('stdout') { should match /^Cookstyle version: #{semver}$/ }
  end
end

describe command 'cinc --help' do
  its('exit_status') { should eq 0 }
  its('stdout') { should_not match /[cC]hef / }
  its('stdout') { should match /cinc command/ }
end

%w(cinc chef).each do |c|
  describe command "#{c} exec gem --version" do
    its('exit_status') { should eq 0 }
  end
end

describe command 'cinc-analyze report --help' do
  its('exit_status') { should eq 0 }
  its('stdout') { should_not match /Chef/ }
  its('stdout') { should match /Generate reports from a Cinc Server/ }
end

describe command 'chef-analyze report --help' do
  its('exit_status') { should eq 0 }
  its('stdout') { should match /Generate reports from a Cinc Server/ }
  its('stdout') { should match /^Redirecting to cinc-analyze...$/ }
end

describe command 'cinc-run --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should_not match /chef-run/ }
  its('stdout') { should match /cinc-run: #{semver}/ }
end

describe command 'chef-run --version' do
  its('exit_status') { should eq 0 }
  its('stdout') { should_not match /chef-run/ }
  its('stdout') { should match /cinc-run: #{semver}/ }
  its('stdout') { should match /^Redirecting to cinc-run...$/ }
end

describe command '/opt/cinc-workstation/embedded/bin/mixlib-install list-versions cinc stable' do
  its('exit_status') { should eq 0 }
end
